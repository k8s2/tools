FROM centos:7

RUN yum -y update && \ 
    yum -y install java-1.8.0-openjdk.x86_64 wget curl python3-pip.noarch unzip groff

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl

RUN wget http://apache.mirror.anlx.net/kafka/2.4.1/kafka_2.13-2.4.1.tgz && \
    tar xvzf kafka_2.13-2.4.1.tgz && \
    rm kafka_2.13-2.4.1.tgz && \
    mv kafka_2.13-2.4.1 kafka

RUN wget https://releases.hashicorp.com/terraform/0.12.23/terraform_0.12.23_linux_amd64.zip && \
    unzip terraform_0.12.23_linux_amd64.zip && \
    mv terraform /usr/local/bin && \
    rm terraform_0.12.23_linux_amd64.zip

RUN pip3 install awscli 

CMD ["/usr/sbin/init"]
